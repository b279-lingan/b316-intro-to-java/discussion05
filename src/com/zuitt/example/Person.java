package com.zuitt.example;

public class Person implements Actions, Greetings {
    //For a class to implement or use an interface, we use the implements keyword
    public void sleep(){
        System.out.println("Zzzzzzzzzz......");
    }

    public void run(){
        System.out.println("Running on the road");
    }

    public void morningGreet(){
        System.out.println("Good Morning, Friend!");
    }

    public void holidayGreet(){
        System.out.println("Happy Holidays, Friend!");
    }

    /*
     * Mini-Activity
     * Create two new actions that a person would do.
     * and Implement them in the actions class.
     * run them in the Main class.
     * */
}
